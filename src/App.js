import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import RouterComponent from './router/router';



const App=()=>{
return (
    <BrowserRouter >
        <RouterComponent /> 
    </BrowserRouter>
);
}

export default App;
