import React from 'react';
import { Route, Switch} from 'react-router-dom';
import ledgerForm from '../component/ledgerForm'
import registerForm from '../component/registerForm'
import success from '../component/sucess'
import commentForm from '../component/comment'
import sample from '../component/sample'
import register from '../component/register'
import replace from '../component/replace'

function RouterComponent() {

  return (
    <div>
      <Switch>
      <Route exact path='/sucess' component={success}></Route>
        <Route path='/ledgerform' component={ledgerForm}></Route>
        <Route path='/comment' component={commentForm}></Route>
        <Route path='/sample' component={sample}></Route>
        <Route path='/register' component={register}></Route>
        <Route path='/replace' component={replace}></Route>
        <Route path='/reg' component={registerForm}></Route>
      </Switch>
    </div>);

}
export default RouterComponent; 
