import React, { Component } from 'react';
import axios from 'axios';

import config from '../config/config';
import '../assets/stylesheet/stylesheet.css';

import Button from '../component/button'
import Input from '../component/formInput';
import Select from '../component/select'

class ledgerForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      date: null,
      effectiveDate: null,
      payee: null,
      status: '',
      account: '',
      amount: '',
      comments: null,
      clicked: false,
      postings: [],
      statusOption: ['Uncleared', 'Cleared', 'Pending'],
    }

  }

  setInputState = (event) => {
    var value = event.target.value;
    if (event.target.name !== 'clicked') {
      this.setState({
        [event.target.name]: value
      })
    }
    else {
      this.setState({
        clicked: !this.state.clicked
      })
    }
  }

  addPostings = () => {
    var posting = {
      "account": this.state.account,
      "amount": this.state.amount,
      "comments": this.state.comments
    }
    this.setState((prevState) => ({
      postings: [...prevState.postings, posting]
    }))
  }

  postForm = (event) => {
    event.preventDefault();
    var formDetails = {
      "date": this.state.date,
      "effectiveDate": this.state.effectiveDate,
      "payee": this.state.payee,
      "status": this.state.status,
      "postings": this.state.postings
    }
    axios.post(`${config.url.baseURL}/form`, { formDetails })
      .then((response) => {
        if (response.status === 200) {
          this.props.history.push({
            pathname: '/register',
          })
        }
      })
      .catch((err) => {
        console.log(err)
      });
  }

  render() {
    return (
      <React.Fragment>
        <div className='inputForm'>
          <div className="card border-light mb-3" id='inputFormCard'>
            <div className="card-header" id='inputFormHeader'>Entry Form</div>
             <div className="card-body border-top border-bottom-0" id='inputFormBody'>
              <form onSubmit={this.postForm}  >
                <Input label='Date' type="date" name="date" className='form-control' handleChange={this.setInputState} placeholder="DD-MM-YYYY" required />
                <Input label='Effective Date' type="date" name="effectiveDate" className='form-control' placeholder="DD-MM-YYYY" handleChange={this.setInputState} />
                <Input label='Payee' type='text' name='payee' className='form-control' handleChange={this.setInputState} placeholder="Enter the Payee" />
                <Select title={'Status'}
                  name={'status'}
                  placeholder={'Select Status'}
                  options={this.state.statusOption}
                  handleChange={this.setInputState}>
                </Select>
                <div>
                  <label>Postings</label><br></br>
                  <Button handleClick={this.setInputState} name='clicked' label='New'></Button>
                  <br></br>
                  {this.state.clicked === true ?
                    <React.Fragment >
                      <Input label='Account' type='text' className='form-control' handleChange={this.setInputState} name='account' placeholder="Enter the Account" />
                      <Input label='Amount' type='text' className='form-control' handleChange={this.setInputState} name='amount' placeholder="Enter Amount" />
                      <Input label='Comments' type='text' className='form-control' handleChange={this.setInputState} name='comments' placeholder="Enter the entry comments" />
                      <Button type='button' handleClick={this.addPostings} label='Submit Posting' className="btn btn-primary" />
                    </React.Fragment> : null}
                </div>
                <br></br>
                <Button type='submit' label='Submit' className="btn btn-primary" />
                <legend></legend>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>);
  }
}

export default ledgerForm
