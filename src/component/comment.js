import React, { Component } from 'react';
import '../assets/stylesheet/stylesheet.css';
import axios from 'axios';
import config from '../config/config';

class commentForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      comments:null,
    }
  }

  postForm(event) {
   event.preventDefault();
   
     axios.post(`${config.url.baseURL}/comment`, `${this.state.comments}`)
       .then(function (response) {
          if(response.status===200)
          {
            this.props.history.push({
              pathname:'/sucess',
          })
          }
      }.bind(this))
      .catch((err) => {
        console.log(err)
      });
  }
  
render() {
  return (
  <div>
    <div className='inputForm'>
      <div className="card border-light mb-3" id='inputFormCard'>
        <div className="card-header" id='inputFormHeader'>Entry Form</div>
          <div className="card-body border-top border-bottom-0" id='inputFormBody'>
            <form  onSubmit={this.postForm.bind(this)}  >
               <div className="form-group">
                <label>Date</label>
                <input type="date" className='form-control' name="date" onChange={event => this.setState({ date: event.target.value })} required />
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>);
}
}
export default commentForm


