import React from 'react';
import PropTypes from 'prop-types';
const Input = ({
    name,
    type,
    placeholder,
    onChange,
    className,
    value,
    error,
    children,
    label,
    ...props
  }) => {
    
    return (
        <div className="form-group">
        <label htmlFor={name}>{label}</label>
        <input
          id={name}
          name={name}
          type={type}
          placeholder={placeholder}
          onChange={props.handleChange}
          value={value}
          className={className}
          style={error && {border: 'solid 1px red'}}
          
        />
        { error && <p>{ error }</p>}
        </div>
    )
  }
  
  Input.defaultProps = {
    type: "text",
    className: ""
  }
  
  Input.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['text', 'number', 'password', 'date', 'email']),
    className: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func
  }

  export default Input