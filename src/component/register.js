import React, { Component } from 'react';
import axios from 'axios';

import config from '../config/config';
import '../assets/stylesheet/stylesheet.css';

import Button from '../component/button'
import Input from '../component/formInput';
import Select from '../component/select'

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            entry: []
        }

    }
    componentDidMount() {
        axios.get(`${config.url.baseURL}/register`)
            .then(response => {
                console.log(response)
                this.setState({
                    entry: response.data
                })
            })
    }
    dateFormat=(date)=>{

        var dateString = date
        var dateObject = new Date(dateString);
        console.log((dateObject.getMonth())+1)
        console.log(dateObject.getDate())
        console.log(dateObject.getFullYear())
        return (`${dateObject.getDate()}-`+`${(dateObject.getMonth())+1}-`+`${dateObject.getFullYear()}`)
    }

    render() {
        console.log(this.state.entry)
        //entry=this.state
        return (
            <React.Fragment>

                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Effective Date</th>
                            <th scope='col'>Code</th>
                            <th scope="col">Status</th>
                            <th scope="col">Payee</th>
                            {/* <th scop="col">Postings</th> */}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.entry.map((entry)=>(
                             <tr key={entry.code}>
                             <td>{this.dateFormat(entry.date)}</td>
                             <td>{entry.effectiveDate !== null? this.dateFormat(entry.effectiveDate):null}</td>
                             <td>{entry.code}</td>
                             <td>{entry.cleared === entry.pending? 'Uncleared' : entry.cleared===true? 'Cleared':'Pending'}</td>
                             <td>{entry.payee}</td>
                             {/* <td>{entry.payee}</td> */}
                            </tr>
                        ))}
                       
                        
                    </tbody>
                </table>
            </React.Fragment>);
    }
}

export default Register
