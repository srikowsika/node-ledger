import React from 'react';

function Button(props) {
return (
    <button type={props.type} onClick={props.handleClick} className={props.className} name={props.name}>{props.label}</button>
);
}
Button.defaultProps = {
    type: "button", 
  } 
export default Button;
